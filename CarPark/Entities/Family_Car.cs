﻿using Entities;

public class Family_Car : AbstractCar
{

    private int number_of_doors;
    private int number_of_seats;

    public int Number_of_doors
    {
        get { return number_of_doors; }
        set { number_of_doors = value; }
    }

    public int Number_of_seats
    {
        get { return number_of_seats; }
        set { number_of_seats = value; }
    }
}
