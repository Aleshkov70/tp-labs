﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace Logic
{
    public class CarParkCalculator
    {
        public int CalculatePrice(CarPark carPark)
        {
            int totalPrice = 0;

            List<AbstractCar> cars = carPark.cars;

            foreach (AbstractCar car in cars)
            {

                totalPrice += car.Price;
            }

            return totalPrice;
        }
    }
}
