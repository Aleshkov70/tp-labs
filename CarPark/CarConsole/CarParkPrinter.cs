﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace Weapon
{
    public class CarParkPrint
    {
        public void Print(CarPark carPark)
        {
            //Вывод на экран
            List<AbstractCar> cars = carPark.cars;

            foreach (AbstractCar car in cars)
            {
                Console.WriteLine("Name: " + car.Brand);
                Console.WriteLine("Price: " + car.Price);
                Console.WriteLine();
            }
        }
    }
}
