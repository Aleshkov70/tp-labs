﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using Logic;

namespace Car
{
    class Program
    {
        static void Main(string[] args)
        {
            CarPark carPark = CarParkFactory.CreateCarPark();
        }
        public class CarParkFactory
        {
            public static CarPark CreateCarPark()
            {
                //object created
                Racing_Car racing_car = new Racing_Car();
                racing_car.Brand = "BMW";
                racing_car.Price = 10000;
                racing_car.Max_speed = 290;
                racing_car.Max_HP = 541;

                Family_Car family_car = new Family_Car();
                family_car.Brand = "Peugeot";
                family_car.Price = 6080;
                family_car.Number_of_doors = 5;
                family_car.Number_of_seats = 8;

                //CarPark created
                CarPark carPark = new CarPark();
                carPark.AddCar(racing_car);
                carPark.AddCar(family_car);

                return carPark;
            }
        }
            CarParkPrinter printer = new CarParkPrinter();
            printer.Print(carPark);

            CarParkCalculator carParkCalculator = new CarParkCalculator();
            int totalPrice = carParkCalculator.CalculatePrice(carPark);

            Console.WriteLine("Total CarPark price " + totalPrice);
            Console.ReadKey();
        }
    }

